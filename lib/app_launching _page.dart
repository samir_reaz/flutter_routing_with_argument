import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:routing_with_argument/home_page.dart';
import 'package:routing_with_argument/person_model.dart';

import 'app_info.dart';

class AppLaunchingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: we can pass a List
    //List<String> name = ['samir', 'pejon'];

    // TODO: we can pass object List
    var personList = List<PersonModel>();
    personList.add(new PersonModel(name: 'Jarin', age: 22, address: 'F'));
    personList.add(new PersonModel(name: 'Tarin', age: 777, address: 'Female'));
    personList.add(new PersonModel(name: 'Saimon', age: 555, address: 'Male'));

    return Scaffold(
      appBar: AppBar(
        title: Text('Launching Page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                //Pushing a route directly, WITHOUT using a named route
                Navigator.of(context).push(
                  // With MaterialPageRoute, you can pass data between pages.
                  // but if you have a more complex app, you will quickly get lost.
                  MaterialPageRoute(
                    builder: (_) => AppInfo(
                      data: 'Mohammad Shah Samir',
                    ),
                  ),
                );
              },
              child: Text('Menual Route'),
            ),
            // TODO : 2 options for named Route navigation:
            // TODO : 1-> Specify a map for routes (Limited Functionality)
            // TODO : 2-> Make a function returning routes (Final Solution)
            RaisedButton(
              onPressed: () {
                Navigator.of(context).pushNamed('/home');
              },
              child: Text('Home'),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.of(context).pushNamed(
                  '/details',
                  // If you pass except String variable then happen an error.
                  // because condition is 'if(args is String)'
                  arguments: personList,
                );
              },
              child: Text('Details'),
            ),
          ],
        ),
      ),
    );
  }
}
