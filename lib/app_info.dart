import 'package:flutter/material.dart';
import 'package:routing_with_argument/selection_screen.dart';

class AppInfo extends StatelessWidget {
  // This is a String for the sake of an example.
  // You can use any type you want.
  final String data;

  AppInfo({
    Key key,
    @required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Info'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("App Info"),
            Text(data),
            RaisedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return SelectionScreen();
                    },
                  ),
                );
              },
              child: Text('Receive info'),
            )
          ],
        ),
      ),
    );
  }
}
