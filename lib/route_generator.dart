import 'package:flutter/material.dart';
import 'package:routing_with_argument/app_launching%20_page.dart';
import 'package:routing_with_argument/details_page.dart';
import 'package:routing_with_argument/home_page.dart';
import 'package:routing_with_argument/person_model.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => AppLaunchingPage());
      case '/home':
        return MaterialPageRoute(builder: (_) => HomePage());
      case '/details':
        // TODO: Validation of correct data type
        if (args is List<PersonModel>) {
          var personList = List<PersonModel>();
          personList = args;
          //print(personList[1].name);
          return MaterialPageRoute(
            builder: (_) => DetailsPage(
              name: personList[0].name,
              age: 55,
              address: personList[1].name,
            ),
          );
          /*return MaterialPageRoute(
              builder: (_) => DetailsPage(personList: personList));*/
        }
        // If args is not of the correct type. return an error page.
        // You can also throw an exception while in development.
        return _errorRoute();
      default:
        // If there is no such named route in the switch statement, e.g. /xyz
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
