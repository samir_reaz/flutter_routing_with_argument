import 'package:flutter/material.dart';

class SelectionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Return Page'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                Navigator.pop(context, 'yep');
              },
              child: Text('Yep'),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.pop(context, 'Nop');
              },
              child: Text('Nop'),
            ),
          ],
        ),
      ),
    );
  }
}
