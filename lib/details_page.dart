import 'package:flutter/material.dart';
import 'package:routing_with_argument/person_model.dart';

class DetailsPage extends StatelessWidget {
  final String name;
  final int age;
  final String address;

  DetailsPage({
    Key key,
    @required this.name,
    @required this.age,
    //set default value. if Constractor don't pass any value then. default value is working.
    this.address = 'empty',
  }) : super(key: key);

  /*var personList = List<PersonModel>();

  DetailsPage({this.personList});*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Details Page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[Text(name), Text(age.toString()), Text(address)],
        ),
      ),

      /*body: ListView.builder(
          itemCount: personList.length,
          itemBuilder: (context, index) {
            final item = personList[index];
            return ListTile(
              title: Text(item.name),
              subtitle: Text(item.address),
            );
          }),*/
    );
  }
}
