import 'package:flutter/cupertino.dart';

class PersonModel {
  final String name;
  final int age;
  final String address;

  PersonModel({this.name, this.age, this.address});
}
